import requests
import time

with open('links') as f:
    links = f.read().split()

now = time.time()
for l in links:
    before = time.time()
    print(f'{before} Making request to {l}')
    res = requests.get(l)
    print(f'Got response {res}')

end = time.time()
took = end - now
print(f"Took {took}")