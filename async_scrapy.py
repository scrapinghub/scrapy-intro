import scrapy
import time


class WikiSpider(scrapy.Spider):
    name = 'wiki_explore'

    def start_requests(self):
        with open('links') as f:
            links = f.read().split()

        for l in links:
            self.logger.info(f'Making request to {l}')
            yield scrapy.Request(l, callback=self.parse_wiki)

    def parse_wiki(self, response):
        self.logger.info(f"Request made {response}")