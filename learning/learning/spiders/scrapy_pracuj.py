import scrapy
import time


class PracujSpider(scrapy.Spider):
    name = 'pracuj'
    start_urls = [
        "https://www.pracuj.pl/praca/python;kw?rd=30"
    ]

    def parse(self, response):
        self.logger.info(f"Request made {response}")
        for link in response.css(".offer-details__title-link::attr(href)").extract():
            yield response.follow(link, callback=self.parse_offer)
            break

    def parse_offer(self, response):
        title = response.css("h1::text").get()
        return {"title": title}