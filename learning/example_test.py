import os

from scrapy.http import HtmlResponse, Response, TextResponse

from learning.spiders.scrapy_pracuj import PracujSpider

def test_sample_offer():
    spider = PracujSpider()
    with open(os.path.join('samples', 'pracuj_sample.html')) as f:
        sample = f.read()

    response = TextResponse('http://url', body=sample, encoding='utf8')
    assert spider.parse_offer(response) == {'title': 'Senior Team Lead Procurement'}